using UnityEngine;
using System.Collections;

public class PrefKeys
{
    protected const string section = "CoasterAR";
    public const string showTutorial = section + ".ShowTutorial";
}
