﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialContoller : MonoBehaviour {
    public GameObject tutorial;

	// Use this for initialization
	void Start () {
        bool shouldShowTutorial = PlayerPrefs.GetString(PrefKeys.showTutorial, "true").Equals("true");
        if(shouldShowTutorial) {
            tutorial.SetActive(true);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void DisableTutorial() {
        PlayerPrefs.SetString(PrefKeys.showTutorial, "false");
        PlayerPrefs.Save();
        tutorial.SetActive(false);
    }
}
