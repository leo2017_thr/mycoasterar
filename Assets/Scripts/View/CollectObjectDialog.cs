﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CollectObjectDialog : MonoBehaviour {
    private MarkCollectedResponse markCollectedResponse;
    private string userName;
    public void SetUserName(string name) { userName = name; }

    public Image image;
    public Image FeatureImage;
    public TextMeshProUGUI featureText;
    public TextMeshProUGUI title;
    public TextMeshProUGUI body;

    public void setMarkCollectedResponse(MarkCollectedResponse response) {
        markCollectedResponse = response;

        StartCoroutine(loadImage(markCollectedResponse.imageUrl, image));
        StartCoroutine(loadImage(markCollectedResponse.featuredImageUrl, FeatureImage));

        featureText.SetText(markCollectedResponse.completedStatus);
        title.SetText("Well done" + (userName.Length > 0 ? ", " + userName : ""));
        body.SetText(markCollectedResponse.details);
    }

    private IEnumerator loadImage(string url, Image destImage) {
        WWW www = new WWW(url);

        yield return www;

        destImage.sprite = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
    }

	// Use this for initialization
	void Start () {
	}

    // Update is called once per frame
    void Update () {
		
	}

    public void dismiss() {
        gameObject.SetActive(false);
    }
}
