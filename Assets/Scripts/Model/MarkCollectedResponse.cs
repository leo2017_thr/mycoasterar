﻿using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public class Coord
{
    public double latitude;
    public double longitude;
}

[Serializable]
public class MarkCollectedResponse {
    public bool isDeleted;
    public DateTime createdOnDateTime;
    public int lastModifiedByUserId;
    public DateTime lastModifiedOnDateTime;
    public int id;
    public int rewardId;
    public object dependsOn;
    public string name;
    public string details;
    public object objectiveType;
    public int radius;
    public int maxRadius;
    public int points;
    public string arId;
    public DateTime validFrom;
    public DateTime validTo;
    public bool active;
    public bool hidden;
    public int createdByUserId;
    public string collectedHeadline;
    public string collectedText;
    public Coord coord;
    public string imageUrl;
    public bool completed;
    public bool rewardCompleted;
    public string featuredImageUrl;
    public string completedStatus;
}
