﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnBack : MonoBehaviour {
    public NativeBridge nativeBridge;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ExitModule();
        }
    }

    public void ExitModule() {
#if UNITY_ANDROID
        nativeBridge.invokeNativeMethod("openTreasureMap");
#endif
    }
}
