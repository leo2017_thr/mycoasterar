﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ObjectBehavior : MonoBehaviour {
    public int id;

    public ApiClient apiClient;

    public GameObject progressBar;

    public CollectObjectDialog collectObjectDialog;

    public CollectObjectDialog completedDialog;

    public GameObject errorDialog;

    void Start()
    {
        Debug.Log("Object1 start");
    }

    public void OnClick()
    {
        Debug.Log("object" + id + "clicked");
        progressBar.SetActive(true);

        apiClient.MarkObjectiveAsCollected(id, (err, response) =>
        {
            progressBar.SetActive(false);

            if (response.statusCode != (int)HttpStatusCode.OK)
            {
                errorDialog.SetActive(true);
            } else
            {
                CollectObjectDialog toShowDialog;
                CollectObjectDialog toHideDialog;

                MarkCollectedResponse markCollectedResponse = JsonUtility.FromJson<MarkCollectedResponse>(response.text);
                if(markCollectedResponse.rewardCompleted) {
                    toShowDialog = completedDialog;
                    toHideDialog = collectObjectDialog;
                } else {
                    toShowDialog = collectObjectDialog;
                    toHideDialog = completedDialog;
                }

                toHideDialog.gameObject.SetActive(false);
                toShowDialog.gameObject.SetActive(true);
                
                toShowDialog.SetUserName(apiClient.GetUserName());
                toShowDialog.setMarkCollectedResponse(markCollectedResponse);
            }
        });
    }
}
