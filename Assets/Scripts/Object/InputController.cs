﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour {
    public ObjectBehavior[] clickObjects;

    private Dictionary<string, ObjectBehavior> objectMap = new Dictionary<string, ObjectBehavior>();

    private void Awake()
    {
        if(clickObjects != null) {
            foreach (ObjectBehavior clickObj in clickObjects) {
                objectMap.Add(clickObj.transform.name, clickObj);
            }
        }
    }

    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                ObjectBehavior obj;
                objectMap.TryGetValue(hit.transform.name, out obj);
                if (obj != null)
                {
                    obj.OnClick();
                }
            }
        }

    }
}
