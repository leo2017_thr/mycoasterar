﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CircularProgressBar : MonoBehaviour {
    public Image LoadingBar;
    float currentValue;
    public float speed;
    public float rotateSpeed;

    // Use this for initialization
    void Start () {
		
	}

    void OnEnable()
    {
        currentValue = 0f;
    }

    // Update is called once per frame
    void Update () {
        currentValue += (speed * Time.deltaTime) % 100;

        if(currentValue >= 100)
        {
            currentValue = 0;
        }

        LoadingBar.fillAmount = currentValue / 100;

        LoadingBar.transform.Rotate(0, 0, -rotateSpeed * Time.deltaTime);
    }
}
