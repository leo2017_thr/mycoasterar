﻿using UnityEngine;
using Vuforia;

public class SweepImageTargetBehavior : MonoBehaviour, ITrackableEventHandler
{
    public GameObject tipsPanel;
    public GameObject caputurePanel;
    public GameObject sweepLogo;
    public GameObject donut;

    private TrackableBehaviour mTrackableBehaviour;

    // Use this for initialization
    void Start()
    {
        mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        tipsPanel.SetActive(true);
        caputurePanel.SetActive(false);

        if (mTrackableBehaviour)
        {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        if (tipsPanel == null)
            return;

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            tipsPanel.SetActive(false);
            caputurePanel.SetActive(false);
            showOneObject();
        }
        else
        {
            tipsPanel.SetActive(true);
            caputurePanel.SetActive(false);
        }
    }

    protected void showOneObject()
    {
        float rand = Random.Range(-1f, 1f);

        if (rand >= 0)
        {
            sweepLogo.SetActive(true);
            donut.SetActive(false);
        }
        else
        {
            sweepLogo.SetActive(false);
            donut.SetActive(true);
        }
    }
}
