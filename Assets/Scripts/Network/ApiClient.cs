﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proyecto26;

public class ApiClient : MonoBehaviour {
    protected string baseUrl;
    protected string userId;
    protected string userName;
    protected string authToken;
    protected string appName;
    protected string appVersion;

    protected Dictionary<string, string> httpHeader;

    public string GetUserName() {
        return userName;
    }

    void Awake() {
        baseUrl = PlayerPrefs.GetString("BaseUrl", "");
        userId = PlayerPrefs.GetString("UserIdReceivedOnLogin", "");
        userName = PlayerPrefs.GetString("UserName", "");
        authToken = PlayerPrefs.GetString("ThredApiServiceUserToken", "");
        appName = PlayerPrefs.GetString("AppName", "");
        appVersion = PlayerPrefs.GetString("AppVersion", "");

#if UNITY_EDITOR
        //only for test with UAT server, please update the authToken if necessary
        baseUrl = "https://test-api.sweep.im/api/v3/";
        userId = "329";
        userName = "Tom";
        authToken = "gAAAAABaZq2SihuqEfSGp3H1qV8DKAQxXdXMA9xbcz-3Gh7riheEUZFc7HAfm_N1UlpjB-1CKDkb19Ajq-AutR3U5kgYKS_WUEbfKkyhH78rR8m3yfhnz6rBj1RT2cxHR2X0XSgziAcfafsbMiDGtFIdtsFMeXMllJJJdFTOHXC-YA5DG5zWPtfB2nFl3dbzzk5aVs5yeeE36zrXLzuzO9uS901gwtIgv-Qv50R48oMvwDylUI8clal-ynbLbDR1NpyD9iIu7AfWKcBoLNduwRPWvgWoQLS9-GujsT0ob30ct0mddjkzclHZqiZL1upCKZ4Ftm7AN_udEX3GabwzA9LAyxyaWAUcRgHfBg0QD6O1rG-RcQUEfi_ck3LdJVNlSFmaUmx6QQ2CEQ6djV28SMLPd8IfXrFg2Mv7i0PfFeY1yJa59s8zwjnkIqBIx2AIHyMt4oa5G1htnKYfpsx5u0iDVwfh1WvBv6vf67ois4fEY2NJlIOUvPiSPU419E_h1MW3cJbnf6wgKW-Kamx8KVPf5EhW-w4zSzcj1JgI8iPi5u7BxOHdIXh2mPL-sxy02xtPh8JsFiWxaV0X3FFc7YC5AvRLFx5M2L764VgLOrWa4rxR0zfaSJno1-LXQWHsjUACRmJhH2KndhBDcsN6ZTIWjj3sXdTC1515qsejGAIR248TjiZh581HQwfx-hvvbNugRvHmiIpFJ__mUQyExmdRq_wxXpvxGryKvAKkKDsUqt6pve-b0qpR6AVQn1fdVh0A7DAXoFajpUFpi6Io_gIHafiBBUrWaudSd0LfKjNASL3rg797KFVXxwjEdjm8GFrUO6qbM6OXRm91KYvA5yO7KMdrRJgWUbBwKSOsBqdxxfdeJCU9o3Ny2-KseA-yp2T6uhm2pF9hIk10aptl8Z2zix0U2VDqzEWSzHw5tKNqKDuYgzMHBhBVFTG389ju3UrbmrPZ-buEvwwrbEH2lTrDDwy0iv9r1Pit5Ds773yEzTM08I86xWCiQrTB8SUkwEbhpifmPnAJsliEmYd4DsbPovaErBAZ2BlKi-GF7pLUJ8eXxOX2FJ7ES9sPC7embEI6Cj1AHDOPJ6YoCeVJLURS7GLH0UTfxWjfQmSS4_IOgnc4MAzUWcsBha24aHCddMoRn0ebTf6rwdGzXamy7VvbeFUIF8ronkCQGq4nHnFHrfh8Yn6GT9RVJ8XQ7hhVaTQfXRlEYNpQb_cLzUGBhxuW-Vgj6XSy3y8Nnn-hhbYBVyIfwVs4yqkjMkv84E9Km3mzNrwNr5DnlO8Mxp7fU326oeFxfIGbmdWDCobDL2OatCvSEkqRvhWrnIUL7_KwlcnOfXzYlMhvIFuumGkXw8GjMTqYZNrGniVS9l8FA-TRkavCU8DoWWv_CFBMOSWro2DBLAi6d25P86mkA4JwajIH_dbjTx11bgqiLWDdplt7a8Jf0NOKk7sg3GV0pOjc7rPxcZplbbRX1qdanJjP33HxfCypqxdW7dKbj8t0baYCqo7oW9a2Gc_rfAyFK3AboQEHmw5m1QWpJ-TSSL3JQvElbxwkqhRCCoFmfUasBw9pbRIJezw6Q6aVfCE77DjUAWixrvhVcRyZlLUk7O0Wqp6w1jSpI4yTesyd4vN3Ui5Kzvu88A0pVutzJ0LsE0ZqHF_VZomLFkfI1_Nud4RLsxwbncCKMcXq_WTzAyIkX-DRnORJuQerRtvQfwBP6LTPP-6bzX6BsjUOcz0ex15KA1NFuaqdfq7A1YFcscERVzLOnCoi-Wdv5QWVV6OdX_XUxUMODZvwAYqaMvQRFPd_BHCYX7RbiJY1Bl5wBDpaJxm_XEbf8isAK8xrHmPUPMI2m70-FwLDInLaJwSn5l6Wkvd_jOQaprdK8aenX2Knn8X007WaZNmFzPMW6LIUcio-avgDZwzzl-zLErAubQjgmOy90N4PzJKkzS2p1n_M68m8tSnTF9hXsmUSdsktS-gRm4wenJf2Ic65Dlm-FIiu2Wawa7xvhV4PjDCk0tyLrobnae-mKq1sD5j9kz5r-sG9so1aS5UPYCoQ5LpA4J4oDiVzlwQcE-27RgTBf_tLENxl3j-Gt5b8mYEMhULEb2V1A2r1uTos9jYoB3wieMhYzLwDcMCsd51xq4BdQLeO6QRpV2FE0fMMgdTDoObbCRB5VoSEu-7ATT-qtAEfclOAn7ueHZm_-xKY6k91n4Nz_HVeo6VePc5UcPoe4yXgLc5-4O-CXU7qJa1Jy1VHES_gJoN6cjCVHsOFAzvDijk4p-hlj3G-tuf9zm313zrrX5cQXnyRWmVWFEa18J4EenBDIy0eX7Hyt2BpErAYqlsjCSZ5aoGEoFB54H_fiVdP_s2t5xOsWvr2oYO4jCnD39a18L0E252v9PMx-9-JcRq0M7_GdlX84aHigqHR8oyTh7h4XvSD21suffyS";
        appName = "Sweep-Android";
        appVersion = "2.0.6.518";
#endif

        httpHeader = new Dictionary<string, string>
        {
            {"Authorization", "Bearer " + authToken},
            {"Sweep-Application-Name", appName },
            {"Sweep-Application-Version", appVersion }
        };
    }

    public void MarkObjectiveAsCollected(int objeciveId, System.Action<System.Exception, ResponseHelper> callback) {
        var requestOptions = new RequestHelper
        {
            url = baseUrl + "users/" + userId + "/objectives/" + objeciveId,
            headers = httpHeader
        };

        RestClient.Post(requestOptions, null, callback);
    }
}
