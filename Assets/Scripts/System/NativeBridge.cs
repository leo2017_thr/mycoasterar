﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NativeBridge : MonoBehaviour {
    public void InviteFriend()
    {
#if UNITY_ANDROID
        invokeNativeMethod("inviteFriends");
#endif
    }

#if UNITY_ANDROID
    public void invokeNativeMethod(string method) {
        if (Application.platform == RuntimePlatform.Android)
        {
            AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");

            activity.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                activity.Call(method);
            }));
        }    
    }
#endif

    public void openTreasureChest()
    {
#if UNITY_ANDROID
        invokeNativeMethod("openTreasureChest");
#endif
    }
}
