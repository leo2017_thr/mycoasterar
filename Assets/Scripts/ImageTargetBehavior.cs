﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using TMPro;

public class ImageTargetBehavior : MonoBehaviour, ITrackableEventHandler {
	public GameObject tipsPanel;
    public GameObject caputurePanel;
    public TextMeshPro textMesh;
    public SwipeModel swipeModel;

	private TrackableBehaviour mTrackableBehaviour;

	// Use this for initialization
	void Start () {
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        tipsPanel.SetActive(true);
        caputurePanel.SetActive(false);

        if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}
	}

	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
        if (tipsPanel == null)
            return;

		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			tipsPanel.SetActive (false);
            caputurePanel.SetActive(true);

            if(swipeModel != null && textMesh != null) {
                int currentActivate = swipeModel.GetCurrentActivate();
                SetLabel(currentActivate);                
            }
		}
		else
		{
			tipsPanel.SetActive (true);
            caputurePanel.SetActive(false);
        }
	} 
	
    public void SetLabel(int currentActivate) {
        switch (currentActivate)
        {
            case 0:
                textMesh.SetText("Corona");
                break;
            case 1:
                textMesh.SetText("Heineken");
                break;
            case 2:
                textMesh.SetText("Hyundai Kona");
                break;
            default:
                textMesh.SetText("Unknown object");
                break;
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
